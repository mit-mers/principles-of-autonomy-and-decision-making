---
layout: page
title: Course Materials
permalink: /course-materials/
---

{% include image.html url="/_images/aima.jpg" caption="Artificial Intelligence: A Modern Approach" width=300 align="right" %}

## Books

- **Artificial Intelligence: A Modern Approach - 4th Edition** by Russell and Norvig. [3rd Edition](https://zoo.cs.yale.edu/classes/cs470/materials/aima2010.pdf)
- **Introduction to Operations Research** by Hiller and Lieberman.
- [**Planning Algorithms**](http://lavalle.pl/planning/book.pdf) by S. LaValle (Cambridge Press), 
- [**Pattern Recognition and Machine Learning**](https://cds.cern.ch/record/998831/files/9780387310732_TOC.pdf) by Christopher M. Bishop (Springer) 2006
- [**OMPL Primer**](https://ompl.kavrakilab.org/OMPL_Primer.pdf)
- **Introduction to Algorithms by Cormen**, Leiserson and Rivest (MIT Press).
- **Constraint Processing** by Rina Dechter (Morgan Kaufmann).
- [**Convex Optimization**](http://stanford.edu/~boyd/cvxbook) by Steven Boyd (Cambridge Press).
